variable "az_subscription_id" {
  type = string
}

variable "resource_group_1_name" {
  type = string
}

variable "resource_group_2_name" {
  type = string
}

variable "location" {
  type = string
}

variable "storage_account_name" {
  type = string
}

variable "storage_account_tier" {
  type = string
}

variable "storage_account_replication_type" {
  type = string
}

variable "service_plan_name" {
  type = string
}

variable "app_service_os_type" {
  type = string
}

variable "app_service_sku_name" {
  type = string
}

variable "function_app_name" {
 type = string
}
