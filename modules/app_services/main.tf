resource "azurerm_service_plan" "app-service-plan-hw" {
  name                = var.service_plan_name
  resource_group_name = var.resource_group_2_name
  location            = var.location
  os_type             = var.app_service_os_type
  sku_name            = var.app_service_sku_name
}

resource "azurerm_linux_function_app" "function-app-hw" {
  name                 = var.function_app_name
  resource_group_name  = var.resource_group_2_name
  location             = var.location
  service_plan_id      = azurerm_service_plan.app-service-plan-hw.id
  storage_account_name = var.storage_account_name
  site_config {}
}
