variable "storage_account_tier" {
  type = string
}

variable "storage_account_replication_type" {
  type = string
}

variable "resource_group_1_name" {
  type = string
}

variable "storage_account_name" {
  type = string
}

variable "location" {
  type = string
}
