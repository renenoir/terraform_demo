resource "azurerm_storage_account" "storage-hw" {
  name                     = var.storage_account_name
  resource_group_name      = var.resource_group_1_name
  location                 = var.location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_replication_type
}
