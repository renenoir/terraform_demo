terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = var.az_subscription_id
}

module "storage-account" {
  source                           = "./modules/storage"
  storage_account_name             = var.storage_account_name
  storage_account_tier             = var.storage_account_tier
  storage_account_replication_type = var.storage_account_replication_type
  resource_group_1_name            = var.resource_group_1_name
  location                         = var.location
}

module "app-services" {
  source                = "./modules/app_services"
  resource_group_2_name = var.resource_group_2_name
  location              = var.location
  service_plan_name     = var.service_plan_name
  app_service_os_type   = var.app_service_os_type
  app_service_sku_name  = var.app_service_sku_name
  function_app_name     = var.function_app_name
  storage_account_name  = module.storage-account.storage.name  
}
